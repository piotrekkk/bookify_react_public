import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { Provider } from 'react-redux';
import configureStore from './config/store';
import './index.css';

const store = configureStore();

const userAuthToken = localStorage.getItem('user_auth_token');

if (userAuthToken) {
  // store.dispatch({ type: AUTHENTICATED });
}

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
