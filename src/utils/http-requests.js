import axios from 'axios';

const authHeaders = () => {
  return {
    'Authorization': localStorage.getItem('user_auth_token') ? `Token token=${localStorage.getItem('user_auth_token')}` : '',
    'Email': localStorage.getItem('user_auth_email') ? localStorage.getItem('user_auth_email') : ''
  };
};

export const post = (url, { params, authenticate: authenticate = true }) => {
  return axios({ method: 'post', url: `${process.env.REACT_APP_API_ENDPOINT}/${url}`, headers: authenticate ? authHeaders() : {}, data: params });
};

export const get = (url, { params, authenticate: authenticate = true } = {}) => {
  return axios({ method: 'get', url: `${process.env.REACT_APP_API_ENDPOINT}/${url}`, headers: authenticate ? authHeaders() : {}, params: params });
};

export const put = (url, { params, authenticate: authenticate = true } = {}) => {
  return axios({ method: 'put', url: `${process.env.REACT_APP_API_ENDPOINT}/${url}`, headers: authenticate ? authHeaders() : {}, data: params });
};

export const destroy = (url, { params, authenticate: authenticate = true } = {}) => {
  return axios({ method: 'delete', url: `${process.env.REACT_APP_API_ENDPOINT}/${url}`, headers: authenticate ? authHeaders() : {}, data: params });
};
