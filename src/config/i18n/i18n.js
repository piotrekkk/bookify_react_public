import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import pl from './pl';
import en from './en';

const resources = { en, pl };

i18n.use(initReactI18next).init({
  resources,
  lng: 'pl',
  keySeparator: false,
  interpolation: {
    escapeValue: false // react already safes from xss
  }
});

export default i18n;
