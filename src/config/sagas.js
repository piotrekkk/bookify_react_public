import { all } from 'redux-saga/effects';
import eventsSaga from '../redux/sagas/events';
import businessTypesSaga from '../redux/sagas/businessTypes';
import sessionSaga from '../redux/sagas/session';
import companySaga from '../redux/sagas/company';
import bootSaga from '../redux/sagas/boot';
import customersSaga from '../redux/sagas/customers';
import servicesSaga from '../redux/sagas/services';
import visitsSaga from '../redux/sagas/visits';

export default function* rootSaga() {
  yield all([
    eventsSaga(),
    businessTypesSaga(),
    sessionSaga(),
    companySaga(),
    bootSaga(),
    customersSaga(),
    servicesSaga(),
    visitsSaga(),
  ]);
}
