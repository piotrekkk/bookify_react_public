import { applyMiddleware, combineReducers, createStore } from 'redux'
import createThunkMiddleware from 'redux-thunk';
import createSagaMiddleware from 'redux-saga'
import servicesReducer from '../redux/reducers/services';
import eventsReducer from '../redux/reducers/events';
import visitsReducer from '../redux/reducers/visits';
import customersReducer from '../redux/reducers/customers';
import companyReducer from '../redux/reducers/company';
import businessTypesReducer from '../redux/reducers/businessTypes';
import sessionReducer from '../redux/reducers/session';
import rootSaga from './sagas';

const thunkMiddleware = createThunkMiddleware;
const sagaMiddleware = createSagaMiddleware();
const middlewares = [thunkMiddleware, sagaMiddleware];

export default function configureStore() {
  const store = createStore(
    combineReducers({
      services: servicesReducer,
      events: eventsReducer,
      visits: visitsReducer,
      customers: customersReducer,
      company: companyReducer,
      businessTypes: businessTypesReducer,
      session: sessionReducer,
    }),
    applyMiddleware(...middlewares)
  );

  sagaMiddleware.run(rootSaga);

  return store;
}
