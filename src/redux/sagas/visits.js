import { put, takeLatest } from 'redux-saga/effects';
import { get, post, put as putRequest } from '../../utils/http-requests';
import { INDEX_VISITS, INDEX_VISITS_SUCCESS, UPDATE_VISIT, UPDATE_VISIT_SUCCESS, CREATE_VISIT, CREATE_VISIT_SUCCESS } from '../reducers/visits';
import { CREATE_CUSTOMER_SUCCESS } from '../reducers/customers';

function* indexVisits() {
  yield put({ type: INDEX_VISITS_SUCCESS, payload: { isLoading: true }});

  const visits = yield get('visits');
  yield put({ type: INDEX_VISITS_SUCCESS, payload: { isLoading: false, ...visits.data }});
}

function* updateVisit({ payload }) {
  yield put({ type: UPDATE_VISIT_SUCCESS, payload: { isUpdating: true, id: payload.visitId }});
  const visit = yield putRequest(`visits/${payload.visitId}`, { params: payload.params });
  yield put({
    type: UPDATE_VISIT_SUCCESS,
    payload: {
      isUpdating: false,
      ...visit.data,
    }
  });
}

function* createVisit({ payload }) {
  yield put({ type: INDEX_VISITS_SUCCESS, payload: { isCreating: true }});

  const visit = yield post('visits', { params: payload.params });
  yield put({ type: INDEX_VISITS_SUCCESS, payload: { isCreating: false }});
  yield put({ type: CREATE_VISIT_SUCCESS, payload: { isLoading: false, ...visit.data }});

  if (payload.isNewCustomer) {
    yield put({ type: CREATE_CUSTOMER_SUCCESS, payload: visit.data.user });
  }
}

export default function* visitsSaga() {
  yield takeLatest(INDEX_VISITS, indexVisits);
  yield takeLatest(UPDATE_VISIT, updateVisit);
  yield takeLatest(CREATE_VISIT, createVisit);
}
