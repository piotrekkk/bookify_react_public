import { put, takeLatest } from 'redux-saga/effects';
import { put as putRequest } from '../../utils/http-requests';
import { UPDATE_COMPANY, UPDATE_COMPANY_SUCCESS } from '../reducers/company';

function* updateCompany({ payload }) {
  yield put({ type: UPDATE_COMPANY_SUCCESS, payload: { isSaving: true }});
  const company = yield putRequest(`companies/${payload.companyId}`, { params: payload.params });
  yield put({ type: UPDATE_COMPANY_SUCCESS, payload: { isSaving: false, ...company.data }});
}

export default function* companySaga() {
  yield takeLatest(UPDATE_COMPANY, updateCompany)
}
