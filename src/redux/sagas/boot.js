import { put, takeLatest } from 'redux-saga/effects';
import { get } from '../../utils/http-requests';
import { GET_BOOT_DATA } from '../reducers/boot';
import { UPDATE_COMPANY_SUCCESS } from '../reducers/company';
import { DELETE_SESSION_SUCCESS } from '../reducers/session';

function* getBootData({ payload }) {
  try {
    const bootData = yield get('boot', { params: payload.params });
    yield put({ type: UPDATE_COMPANY_SUCCESS, payload: bootData.data.company });
  } catch (error) {
    yield put({ type: DELETE_SESSION_SUCCESS });
  }
}

export default function* bootSaga() {
  yield takeLatest(GET_BOOT_DATA, getBootData)
}
