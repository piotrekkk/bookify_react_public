import { put, takeLatest } from 'redux-saga/effects';
import { get } from '../../utils/http-requests';
import { INDEX_EVENTS, INDEX_EVENTS_SUCCESS } from '../reducers/events';

function* indexEvents() {
  const events = yield get('events');
  yield put({ type: INDEX_EVENTS_SUCCESS, payload: events.data });
}

export default function* eventsSaga() {
  yield takeLatest(INDEX_EVENTS, indexEvents)
}
