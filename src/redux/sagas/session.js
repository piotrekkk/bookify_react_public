import { put, takeLatest } from 'redux-saga/effects';
import { destroy, post } from '../../utils/http-requests';
import { CREATE_SESSION, CREATE_SESSION_SUCCESS, DELETE_SESSION, DELETE_SESSION_SUCCESS } from '../reducers/session';

function* createSession({ payload }) {
  try {
    const session = yield post('sessions', payload);

    localStorage.setItem('user_auth_token', session.data.auth_token);
    localStorage.setItem('user_auth_email', session.data.email);

    yield put({ type: CREATE_SESSION_SUCCESS, payload: session.data });
  } catch(error) {
  }
}

function* deleteSession() {
  yield destroy('sessions');

  localStorage.removeItem('user_auth_token');
  localStorage.removeItem('user_auth_email');

  yield put({ type: DELETE_SESSION_SUCCESS });
}

export default function* sessionSaga() {
  yield takeLatest(CREATE_SESSION, createSession);
  yield takeLatest(DELETE_SESSION, deleteSession);
}
