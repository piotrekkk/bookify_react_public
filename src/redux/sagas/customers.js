import { put, takeLatest } from 'redux-saga/effects';
import { get } from '../../utils/http-requests';
import { INDEX_CUSTOMERS, INDEX_CUSTOMERS_SUCCESS } from '../reducers/customers';

function* indexCustomers() {
  yield put({ type: INDEX_CUSTOMERS_SUCCESS, payload: { isLoading: true }});

  const customers = yield get('customers');
  yield put({ type: INDEX_CUSTOMERS_SUCCESS, payload: { isLoading: false, ...customers.data }});
}

export default function* customersSaga() {
  yield takeLatest(INDEX_CUSTOMERS, indexCustomers);
}
