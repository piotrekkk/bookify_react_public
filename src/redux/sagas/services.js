import { put, takeLatest } from 'redux-saga/effects';
import { get, post, put as putRequest } from '../../utils/http-requests';
import {
  INDEX_SERVICES,
  INDEX_SERVICES_SUCCESS,
  UPDATE_SERVICE,
  UPDATE_SERVICE_SUCCESS,
  CREATE_SERVICE,
  CREATE_SERVICE_SUCCESS,
} from '../reducers/services';

function* indexServices() {
  yield put({ type: INDEX_SERVICES_SUCCESS, payload: { isLoading: true }});

  const services = yield get('services');
  yield put({ type: INDEX_SERVICES_SUCCESS, payload: { isLoading: false, ...services.data }});
}

function* updateService({ payload }) {
  yield put({ type: UPDATE_SERVICE_SUCCESS, payload: { isUpdating: true, id: payload.serviceId }});
  const service = yield putRequest(`services/${payload.serviceId}`, { params: payload.params });
  yield put({
    type: UPDATE_SERVICE_SUCCESS,
    payload: {
      isUpdating: false,
      ...service.data,
    }
  });
}

function* createService({ payload }) {
  yield put({ type: INDEX_SERVICES_SUCCESS, payload: { isCreating: true }});

  const service = yield post('services', { params: payload.params });
  yield put({ type: INDEX_SERVICES_SUCCESS, payload: { isCreating: false }});
  yield put({ type: CREATE_SERVICE_SUCCESS, payload: { isLoading: false, ...service.data }});
}

export default function* servicesSaga() {
  yield takeLatest(INDEX_SERVICES, indexServices);
  yield takeLatest(UPDATE_SERVICE, updateService);
  yield takeLatest(CREATE_SERVICE, createService);
}
