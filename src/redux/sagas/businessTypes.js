import { put, takeLatest } from 'redux-saga/effects';
import { get } from '../../utils/http-requests';
import { INDEX_BUSINESS_TYPES, INDEX_BUSINESS_TYPES_SUCCESS } from '../reducers/businessTypes';

function* indexBusinessTypes() {
  const businessTypes = yield get('business_types');
  yield put({ type: INDEX_BUSINESS_TYPES_SUCCESS, payload: businessTypes.data });
}

export default function* businessTypesSaga() {
  yield takeLatest(INDEX_BUSINESS_TYPES, indexBusinessTypes)
}
