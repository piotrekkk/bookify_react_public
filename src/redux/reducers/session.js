const sessionState = {
  isLoggedIn: null,
  user: {}
};

export const CREATE_SESSION = 'CREATE_SESSION';
export const CREATE_SESSION_SUCCESS = 'CREATE_SESSION_SUCCESS';
export const DELETE_SESSION = 'DELETE_SESSION';
export const DELETE_SESSION_SUCCESS = 'DELETE_SESSION_SUCCESS';

export default (state = sessionState, action) => {
  const { type, payload } = action;

  switch (type) {
    case CREATE_SESSION_SUCCESS:
      return {
        isLoggedIn: true,
        user: {
          ...payload
        }
      };
    case DELETE_SESSION_SUCCESS:
      return {
        isLoggedIn: false
      };
    default:
      return state;
  }
}
