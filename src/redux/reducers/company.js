const companyState = {
  isSaving: false,
};

export const UPDATE_COMPANY = 'UPDATE_COMPANY';
export const UPDATE_COMPANY_SUCCESS = 'UPDATE_COMPANY_SUCCESS';

export default (state = companyState, action) => {
  const { type, payload } = action;

  switch (type) {
    case UPDATE_COMPANY_SUCCESS:
      return {
        ...state,
        ...payload
      };
    default:
      return state;
  }
}
