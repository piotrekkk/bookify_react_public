const customersState = {
  isLoading: false,
  total_size: 0,
  page: null,
  page_size: 0,
  collection: []
};

export const INDEX_CUSTOMERS = 'INDEX_CUSTOMERS';
export const INDEX_CUSTOMERS_SUCCESS = 'INDEX_CUSTOMERS_SUCCESS';
export const CREATE_CUSTOMER_SUCCESS = 'CREATE_CUSTOMER_SUCCESS';

export default (state = customersState, action) => {
  const { type, payload } = action;

  switch (type) {
    case INDEX_CUSTOMERS_SUCCESS: {
      return {
        ...state,
        ...payload,
      };
    }
    case CREATE_CUSTOMER_SUCCESS: {
      return {
        ...state,
        total_size: state.total_size++,
        collection: [...state.collection, payload]
      };
    }
    default:
      return state;
  }
}
