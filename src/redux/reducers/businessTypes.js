const businessTypesState = {
  total_size: 0,
  page: null,
  page_size: 0,
  collection: []
};

export const INDEX_BUSINESS_TYPES = 'INDEX_BUSINESS_TYPES';
export const INDEX_BUSINESS_TYPES_SUCCESS = 'INDEX_BUSINESS_TYPES_SUCCESS';

export default (state = businessTypesState, action) => {
  const { type, payload } = action;

  switch (type) {
    case INDEX_BUSINESS_TYPES_SUCCESS:
      return {
        total_size: payload.total_size,
        page: payload.page,
        page_size: payload.page_size,
        collection: payload.collection,
      };
    default:
      return state;
  }
}
