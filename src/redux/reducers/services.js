const servicesState = {
  isLoading: false,
  isCreating: false,
  total_size: null,
  page: null,
  page_size: null,
  collection: []
};

export const INDEX_SERVICES = 'INDEX_SERVICES';
export const INDEX_SERVICES_SUCCESS = 'INDEX_SERVICES_SUCCESS';
export const UPDATE_SERVICE = 'UPDATE_SERVICE';
export const UPDATE_SERVICE_SUCCESS = 'UPDATE_SERVICE_SUCCESS';
export const CREATE_SERVICE = 'CREATE_SERVICE';
export const CREATE_SERVICE_SUCCESS = 'CREATE_SERVICE_SUCCESS';

export default (state = servicesState, action) => {
  const { type, payload } = action;

  switch (type) {
    case INDEX_SERVICES_SUCCESS:
      return {
        ...state,
        ...payload,
      };
    case UPDATE_SERVICE_SUCCESS: {
      const collection = state.collection.map(service => {
        if (service.id === payload.id) {
          return {
            ...service,
            ...payload,
          };
        } else {
          return service;
        }
      });

      return {
        ...state,
        collection,
      }
    }
    case CREATE_SERVICE_SUCCESS: {
      return {
        ...state,
        total_size: state.total_size++,
        collection: [...state.collection, payload]
      };
    }
    default:
      return state;
  }
}
