import moment from 'moment';

const eventsState = [];

export const INDEX_EVENTS = 'INDEX_EVENTS';
export const INDEX_EVENTS_SUCCESS = 'INDEX_EVENTS_SUCCESS';

export default (state = eventsState, action) => {
  const { type, payload } = action;

  switch (type) {
    case INDEX_EVENTS_SUCCESS: {
      const events = payload.map(({ id, title, allDay, start, end  }) => {
        return {
          id,
          title,
          allDay,
          start: moment(start).toDate(),
          end: moment(end).toDate()
        };
      });
      return [...events];
    }
    case 'DELETE_EVENT': {
      return state.filter(event => event.id !== action.state.visitId);
    }
    default:
      return state;
  }
}
