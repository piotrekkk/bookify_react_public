const visitsState = {
  isLoading: false,
  isCreating: false,
  total_size: null,
  page: null,
  page_size: null,
  collection: []
};

export const INDEX_VISITS = 'INDEX_VISITS';
export const INDEX_VISITS_SUCCESS = 'INDEX_VISITS_SUCCESS';
export const UPDATE_VISIT = 'UPDATE_VISIT';
export const UPDATE_VISIT_SUCCESS = 'UPDATE_VISIT_SUCCESS';
export const CREATE_VISIT = 'CREATE_VISIT';
export const CREATE_VISIT_SUCCESS = 'CREATE_VISIT_SUCCESS';

export default (state = visitsState, action) => {
  const { type, payload } = action;

  switch (type) {
    case INDEX_VISITS_SUCCESS:
      return {
        ...state,
        ...payload,
      };
    case UPDATE_VISIT_SUCCESS: {
      const collection = state.collection.map(visit => {
        if (visit.id === payload.id) {
          return {
            ...visit,
            ...payload,
          };
        } else {
          return visit;
        }
      });

      return {
        ...state,
        collection,
      }
    }
    case CREATE_VISIT_SUCCESS: {
      return {
        ...state,
        total_size: state.total_size++,
        collection: [...state.collection, payload]
      };
    }
    default:
      return state;
  }
}
