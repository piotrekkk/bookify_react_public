import { GET_BOOT_DATA } from '../reducers/boot';

export const getBootDataAction = payload => {
  return {
    type: GET_BOOT_DATA,
    payload
  }
}
