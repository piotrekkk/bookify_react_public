import { UPDATE_COMPANY } from '../reducers/company';

export const updateCompanyAction = payload => {
  return {
    type: UPDATE_COMPANY,
    payload
  }
}
