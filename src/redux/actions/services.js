import { INDEX_SERVICES, UPDATE_SERVICE, CREATE_SERVICE } from '../reducers/services';

export const indexServicesAction = () => {
  return {
    type: INDEX_SERVICES
  }
}

export const updateServiceAction = payload => {
  return {
    type: UPDATE_SERVICE,
    payload
  }
}

export const createServiceAction = payload => {
  return {
    type: CREATE_SERVICE,
    payload
  }
}
