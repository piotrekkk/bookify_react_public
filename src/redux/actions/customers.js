import { INDEX_CUSTOMERS } from '../reducers/customers';

export const indexCustomersAction = () => {
  return {
    type: INDEX_CUSTOMERS
  }
}
