import { INDEX_EVENTS } from '../reducers/events';

export const indexEventsAction = () => {
  return {
    type: INDEX_EVENTS
  }
}
