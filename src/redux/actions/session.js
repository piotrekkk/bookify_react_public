import { CREATE_SESSION, DELETE_SESSION } from '../reducers/session';

export const createSessionAction = payload => {
  return {
    type: CREATE_SESSION,
    payload
  }
}

export const deleteSessionAction = () => {
  return {
    type: DELETE_SESSION
  }
}
