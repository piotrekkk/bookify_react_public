import { INDEX_BUSINESS_TYPES } from '../reducers/businessTypes';

export const indexBusinessTypesAction = () => {
  return {
    type: INDEX_BUSINESS_TYPES
  }
}
