import { INDEX_VISITS, UPDATE_VISIT, CREATE_VISIT } from '../reducers/visits';

export const indexVisitsAction = () => {
  return {
    type: INDEX_VISITS
  }
}

export const updateVisitAction = payload => {
  return {
    type: UPDATE_VISIT,
    payload
  }
}

export const createVisitAction = payload => {
  return {
    type: CREATE_VISIT,
    payload
  }
}
