import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import InnerApp from './components/App/InnerApp';
import SignIn from './components/Landing/SignIn';
import './style/App.scss';
import './config/i18n/i18n';

import dotenv from 'dotenv';
dotenv.config();

const App = () => {
  if (window.location.pathname === '/') {
    const isLoggedIn = localStorage.getItem('user_auth_token') && localStorage.getItem('user_auth_email');

    if (isLoggedIn) {
      window.location.replace(`${window.location.origin}/app/dashboard`);
    } else {
      window.location.replace(`${window.location.origin}/signin`);
    }
  }

  return (
    <Router>
      <Route path="/app" component={InnerApp} />
      <Route path="/signin" component={SignIn} />
    </Router>
  );
};

export default App;
