import React from 'react'

const FormDisplay = props => {
  const { labelClassName, name, value } = props;

  return (
    <label className={['ui-form-input', labelClassName].join(' ')}>
      <span className="ui-form-input__title">{name}</span>
      <span className="">{value}</span>
    </label>
  );
};

export default FormDisplay;
