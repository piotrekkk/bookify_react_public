import React, { Component } from 'react'

class Loader extends Component {
  render() {
    const { size, color, wrapperColor, isStatic } = this.props;

    return (
      <div className={`ui-loader ui-loader--size-${size} ui-loader--color-${color} ui-loader--background-color-${wrapperColor} ${isStatic ? 'ui-loader--static' : ''}`}>
        <div className="ui-loader_wrapper">
          <div className="ui-loader_dot"></div>
          <div className="ui-loader_dot"></div>
          <div className="ui-loader_dot"></div>
          <div className="ui-loader_dot"></div>
        </div>
      </div>
    );
  }
}

export default Loader;
