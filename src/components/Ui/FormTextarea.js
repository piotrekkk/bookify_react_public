import React from 'react'

const FormTextarea = props => {
  const { labelClassName, name, value, onChange } = props;
  const placeholder = props.placeholder || name;

  return (
    <label className={['ui-form-input', labelClassName].join(' ')}>
      <span className="ui-form-input__title">{name}</span>
      <textarea
        className="input input--textarea"
        placeholder={placeholder}
        value={value || ''}
        onChange={(e) => onChange(e.target.value)}
      />
    </label>
  );
};

export default FormTextarea;
