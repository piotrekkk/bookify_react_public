import React from 'react'

const PageTitle = props => {
  const { title, rightContent } = props;

  return (
    <div className="ui-page-title">
      <h1 className="ui-page-title__text">{title}</h1>
      {rightContent}
    </div>
  );
};

export default PageTitle;
