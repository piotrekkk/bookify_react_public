import React from 'react'
import { Link } from 'react-router-dom';

const FormLink = props => {
  const { labelClassName, name, value, to } = props;

  return (
    <label className={['ui-form-input', labelClassName].join(' ')}>
      <span className="ui-form-input__title">{name}</span>
      <Link to={to}>{value}</Link>
    </label>
  );
};

export default FormLink;
