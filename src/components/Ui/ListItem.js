import React from 'react'
import { Link } from 'react-router-dom';

const ListItem = props => {
  const { children, to, size, type } = props;
  const isSmallSize = size && size === 'small';
  const isError = type === 'error';
  const isAlert = type === 'alert';

  return (
    <Link
      className={[
        'ui-list-item',
        isSmallSize && 'ui-list-item--small',
        isError && 'ui-list-item--error',
        isAlert && 'ui-list-item--alert',
      ].join(' ')}
      to={to}
    >
      {children}
    </Link>
  );
};

export default ListItem;
