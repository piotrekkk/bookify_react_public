import React from 'react'

const SlotPicker = props => {
  const { slots: days = [], selected, onChange } = props;

  return (
    <div className="ui-slot-picker">
      { days.map((dayObject, index) =>
        <div key={index} className="ui-slot-picker__day-column">
          <span className="ui-slot-picker__day-column__name">{dayObject.day}</span>
          { dayObject.slots.length &&
            <ul>
              { dayObject.slots.map((hour, index) =>
                <li
                  className={['ui-slot-picker__day-column__slot', (dayObject.day === selected.day && hour === selected.hour) && 'ui-slot-picker__day-column__slot--selected'].join(' ')}
                  key={index}
                  onClick={() => onChange(dayObject.day, hour)}
                >
                  {hour}
                </li>
              )}
            </ul>
          }
        </div>
      ) }
    </div>
  );
};

export default SlotPicker;
