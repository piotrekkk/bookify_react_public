import React from 'react'

const Select = props => {
  const { onChange, options, value, placeholder, disabled = false } = props;

  return (
    <div className="ui-select">
      <select disabled={disabled} className="ui-select__select" value={value} onChange={(e) => onChange(e.target.value)}>
        <option value={null}>{placeholder}</option>
        {options && options.map((option, index) =>
          <option key={index} value={option.value}>{option.name}</option>
        )}
      </select>
    </div>
  );
};

export default Select;
