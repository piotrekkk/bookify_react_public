import React from 'react'

const FormInput = props => {
  const { labelClassName, name, value, defaultValue, onChange } = props;
  const type = props.type || 'text';
  const placeholder = props.placeholder || name;

  return (
    <label className={['ui-form-input', labelClassName].join(' ')}>
      <span className="ui-form-input__title">{name}</span>
      <input
        type={type}
        className="input"
        placeholder={placeholder}
        {...(props.value && { value })}
        {...(props.defaultValue && { defaultValue })}
        defaultValue={defaultValue}
        onChange={(e) => onChange(e.target.value)}
      />
    </label>
  );
};

export default FormInput;
