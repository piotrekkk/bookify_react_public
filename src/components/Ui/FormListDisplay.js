import React from 'react'
import ListItem from './ListItem';

const FormListDisplay = props => {
  const { labelClassName, name, items, to } = props;

  return (
    <label className={['ui-form-list-display', 'ui-form-input', labelClassName].join(' ')}>
      <span className="ui-form-input__title">{name}</span>
      <div className="ui-form-list-display__list">
        { items && items.map((item, index) =>
          <ListItem key={index} size="small" to={`${to}/${item.id}`}>{ item.name }</ListItem>
        )}
      </div>
    </label>
  );
};

export default FormListDisplay;
