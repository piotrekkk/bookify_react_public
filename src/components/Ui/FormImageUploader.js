import React from 'react'
import ImageUploader from './ImageUploader';

const FormImageUploader = props => {
  const { name, dirName, filePrefix, imageUrl, onUploadFinished } = props;

  return (
    <label className="ui-form-input">
      <span className="ui-form-input__title">{name}</span>
      <ImageUploader
        dirName={dirName}
        filePrefix={filePrefix}
        imageUrl={imageUrl}
        onUploadFinished={onUploadFinished}
      />
    </label>
  );
};

export default FormImageUploader;
