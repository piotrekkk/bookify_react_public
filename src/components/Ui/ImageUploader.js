import React, { Component } from 'react'
import Loader from './Loader';
import S3 from 'aws-s3';

class ImageUploader extends Component {
  state = {
    isUploading: false
  }

  constructor(props) {
    super(props);

    const config = {
        bucketName: 'bookify-content',
        dirName: 'company-images',
        region: 'eu-west-1',
        accessKeyId: process.env.REACT_APP_AWS_S3_ACCESS_KEY_ID,
        secretAccessKey: process.env.REACT_APP_AWS_S3_ACCESS_KEY_SECRET,
    };
    this.s3Client = new S3(config);
  }

  onUpload = async (event) => {
    this.setState({ isUploading: true });
    const [file] = event.target.files;

    try {
      const data = await this.uploadToS3(file);
      await this.props.onUploadFinished(data.location);
      this.setState({ isUploading: false });
    } catch (error) {
    }
  }

  uploadToS3(file) {
    const fileName = `${this.props.filePrefix}`;
    return this.s3Client.uploadFile(file, fileName);
  }

  render() {
    const { imageUrl } = this.props;
    const { isUploading } = this.state;
    const backgroundStyle = imageUrl ? { backgroundImage: `url(${encodeURI(imageUrl)})` } : {};

    return (
      <div className="ui-image-uploader">
        { isUploading ?
          <Loader size="medium" color="purple" wrapperColor="white" />
        :
          <div className="ui-image-uploader__image" style={backgroundStyle}></div>
        }
        <input type="file" onChange={this.onUpload} />
      </div>
    );
  }
}

export default ImageUploader;
