import React from 'react'

const AlertBox = props => {
  const { text, type, baseClassName } = props;
  const isError = type === 'error';

  return (
    <div className={['ui-alert-box', isError && 'ui-alert-box--error', baseClassName].join(' ')}>
      <span>{text}</span>
    </div>
  )
}

export default AlertBox;
