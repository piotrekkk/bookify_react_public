import React from 'react'

const ListItemsHeader = props => {
  const { children } = props;

  return (
    <div className="ui-list-items-header">
      {children}
    </div>
  );
};

export default ListItemsHeader;
