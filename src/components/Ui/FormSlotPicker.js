import React from 'react'
import SlotPicker from './SlotPicker';

const FormSlotPicker = props => {
  const { name, slots, selected, onChange } = props;

  return (
    <label className="ui-form-input">
      <span className="ui-form-input__title">{name}</span>
      <SlotPicker slots={slots} selected={selected} onChange={onChange} />
    </label>
  );
};

export default FormSlotPicker;
