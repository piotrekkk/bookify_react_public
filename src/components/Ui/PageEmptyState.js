import React from 'react';

const PageEmptyState = props => {
  const { title, description, image } = props;

  return (
    <div className="ui-page-empty-state">
      { image && image}
      { title && <h2>{title}</h2>}
      { description && <p>{description}</p>}
    </div>
  );
};

export default PageEmptyState;
