import React from 'react'
import Select from './Select';

const FormSelect = props => {
  const { labelClassName, name, value, onChange, options, placeholder, disabled, rightContent } = props;

  return (
    <label className={['ui-form-input', labelClassName].join(' ')}>
      <div className="ui-form-input__title">
        <span>{name}</span>
        {rightContent}
      </div>
      <Select value={value} options={options} onChange={onChange} placeholder={placeholder} disabled={disabled} />
    </label>
  );
};

export default FormSelect;
