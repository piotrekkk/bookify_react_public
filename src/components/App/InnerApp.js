import React, { Component } from 'react'
import Navigation from './Navigation';
import Settings from './Settings/Settings';
import Dashboard from './Dashboard/Dashboard';
import Customers from './Customers/Customers';
import Visits from './Visits/Visits';
import { Route } from 'react-router-dom';
import { connect } from 'react-redux';
import { getBootDataAction } from '../../redux/actions/boot';

class InnerApp extends Component {
  componentWillMount() {
    const userAuthToken = localStorage.getItem('user_auth_token');
    const userAuthEmail = localStorage.getItem('user_auth_email');

    if (!userAuthToken || !userAuthEmail) {
      this.props.history.push('/signin');
    } else {
      this.props.getBootData({
        params: {
          user_auth_token: userAuthToken,
          user_auth_email: userAuthEmail
        }
      });
    }
  }

  render() {
    return (
      <div className="c-inner-app">
        <Navigation />
        <Route path="/app/dashboard" component={Dashboard} />
        <Route path="/app/customers" component={Customers} />
        <Route path="/app/Visits" component={Visits} />
        <Route path="/app/settings" component={Settings} />
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  getBootData: data => dispatch(getBootDataAction(data)),
});

export default connect(null, mapDispatchToProps)(InnerApp);
