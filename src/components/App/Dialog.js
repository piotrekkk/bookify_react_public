import React, { Component } from 'react'
import Loader from '../Ui/Loader';
import { withRouter } from 'react-router-dom';
import CloseSvg from '../Ui/Svg/CloseSvg';

class Dialog extends Component {
  goBack = () => {
    this.props.history.push(this.props.exitRoute);
  }

  render() {
    const { isLoadingContent } = this.props;

    return (
      <div className="c-dialog">
        <div className="c-dialog__close-wrapper" onClick={() => this.goBack()}></div>

        <div className={`c-dialog__wrapper ${this.props.wrapperClass}`}>

          { isLoadingContent && <Loader size="medium" color="purple" wrapperColor="white" /> }

          <header className="c-dialog__wrapper__header">
            {this.props.header}
            <CloseSvg className="c-dialog__wrapper__header__close-button" onClick={this.goBack} />
          </header>

          <div className="c-dialog__wrapper__inner">
            {this.props.children}
          </div>

          <div className="c-dialog__wrapper__buttons">
            <button className="button button--medium button--transparent" onClick={this.goBack}>Anuluj</button>
            {this.props.actionButton}
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(Dialog);
