import React, { Component } from 'react'
import PageTitle from '../../Ui/PageTitle';
import { connect } from 'react-redux';
import { indexVisitsAction } from '../../../redux/actions/visits';
import ListItem from '../../Ui/ListItem';
import ListItemsHeader from '../../Ui/ListItemsHeader';
import Loader from '../../Ui/Loader';
import { Link, Route } from 'react-router-dom';
import VisitDialog from '../Dialogs/VisitDialog';
import NewVisitDialog from '../Dialogs/NewVisitDialog';
import PageEmptyState from '../../Ui/PageEmptyState';
import VisitsEmptySvg from '../../Ui/Svg/VisitsEmptySvg';
import moment from 'moment';

class Visits extends Component {
  componentDidMount() {
    this.props.indexVisits();
  }

  getVisitItemType(visit) {
    if (visit.canceled_at) return 'error';
    if (visit.is_past) return 'alert';
    return '';
  }

  getStatusText(visit) {
    if (visit.canceled_at) return 'Odwołana';
    if (visit.is_past) return 'Przeszła';
    return '';
  }

  render() {
    const { visits } = this.props;

    return (
      <div className="c-page">
        <PageTitle
          title="Wizyty"
          rightContent={<Link className="button button--medium button--filled-purple" to="/app/visits/visit/new">+ Dodaj</Link>}
        />

        { !visits.isLoading && visits.collection.length > 0 &&
          <ListItemsHeader>
            <span>Klient</span>
            <span>Usługa</span>
            <span>Data</span>
            <span>Status</span>
          </ListItemsHeader>
        }

        { !visits.isLoading && visits.collection.length === 0 &&
          <PageEmptyState
            title="Nie masz jeszcze żadnych wizyt!"
            description="Bądź cierpliwy i poczekaj na swoją pierwszą wizytę."
            image={<VisitsEmptySvg style={{width: '100%', maxWidth: 500}} />}
          />
        }

        { (visits.isLoading && !visits.collection.length) && <Loader size="medium" color="purple" wrapperColor="white" isStatic={true} /> }

        { visits.collection && visits.collection.sort((a, b) => new Date(b.date) - new Date(a.date)).map(visit =>
            <ListItem key={visit.id} to={`/app/visits/${visit.id}`} type={this.getVisitItemType(visit)}>
              <span>{ visit.user.first_name } { visit.user.last_name }</span>
              <span>{ visit.service.name }</span>
              <span>{ moment(visit.date).format('LLLL') }</span>
              <span>{ this.getStatusText(visit) }</span>
            </ListItem>
          )
        }

        <Route path="/app/visits/visit/new" component={NewVisitDialog} />
        <Route path="/app/visits/:visitId" component={VisitDialog} exact={true} />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  visits: state.visits
});

const mapDispatchToProps = dispatch => ({
  indexVisits: () => dispatch(indexVisitsAction())
});

export default connect(mapStateToProps, mapDispatchToProps)(Visits);
