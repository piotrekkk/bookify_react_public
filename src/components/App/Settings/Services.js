import React, { Component } from 'react'
import { Link, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import { indexServicesAction } from '../../../redux/actions/services';
import NewServiceDialog from '../Dialogs/NewServiceDialog';
import ServiceDialog from '../Dialogs/ServiceDialog';
import ListItem from '../../Ui/ListItem';
import ListItemsHeader from '../../Ui/ListItemsHeader';
import Loader from '../../Ui/Loader';
import PageEmptyState from '../../Ui/PageEmptyState';
import ServicesEmptySvg from '../../Ui/Svg/ServicesEmptySvg';

class Services extends Component {
  componentDidMount() {
    this.props.indexServices();
  }

  render() {
    const { services } = this.props;

    return (
      <>
        <div className="c-settings__content__header">
          <h2 className="c-settings__content__header">Usługi</h2>
          <Link className="button button--medium button--filled-purple" to="/app/settings/services/service/new">+ Dodaj</Link>
        </div>

        { !services.isLoading && services.collection.length > 0 &&
          <ListItemsHeader>
            <span>Nazwa</span>
            <span>Cena</span>
            <span>Czas trwania</span>
          </ListItemsHeader>
        }

        { !services.isLoading && services.collection.length === 0 &&
          <PageEmptyState
            title="Nie masz jeszcze żadnych usług!"
            description="Dodaj usługi klikając w przycisk powyżej."
            image={<ServicesEmptySvg style={{width: '100%', maxWidth: 500}} />}
          />
        }

        { (services.isLoading && !services.collection.length) && <Loader size="medium" color="purple" wrapperColor="white" isStatic={true} /> }

        { services.collection.map((service, index) =>
            <ListItem key={index} to={`/app/settings/services/${service.id}`}>
              <span>{ service.name }</span>
              <span>{ service.price } zł</span>
              <span>{ service.duration } min</span>
            </ListItem>
          )
        }

        <Route path="/app/settings/services/service/new" component={NewServiceDialog} />
        <Route path="/app/settings/services/:serviceId" component={ServiceDialog} exact={true} />
      </>
    );
  }
}

const mapStateToProps = state => ({
  services: state.services
});

const mapDispatchToProps = dispatch => ({
  indexServices: data => dispatch(indexServicesAction(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(Services);
