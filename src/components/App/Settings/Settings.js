import React, { Component } from 'react'
import { Route, NavLink } from 'react-router-dom';
import General from './General';
import Services from './Services';
import PageTitle from '../../Ui/PageTitle';

class Settings extends Component {
  render() {
    return (
      <div className="c-page c-settings">
        <PageTitle title="Ustawienia" />

        <div className="c-settings__wrapper">
          <ul className="c-settings__menu">
            <li>
              <NavLink to="/app/settings/general" className="c-settings__menu__item" activeClassName="c-settings__menu__item--selected">Ogólne</NavLink>
            </li>
            <li>
              <NavLink to="/app/settings/services" className="c-settings__menu__item" activeClassName="c-settings__menu__item--selected">Usługi</NavLink>
            </li>
          </ul>

          <div className="c-settings__content">
            <Route path="/app/settings/general" component={General} />
            <Route path="/app/settings/services" component={Services} />
          </div>
        </div>
      </div>
    );
  }
}

export default Settings
