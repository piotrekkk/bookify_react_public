import React, { Component } from 'react'
import FormInput from '../../Ui/FormInput';
import FormSelect from '../../Ui/FormSelect';
import FormTextarea from '../../Ui/FormTextarea';
import FormImageUploader from '../../Ui/FormImageUploader';
import Loader from '../../Ui/Loader';
import { connect } from 'react-redux';
import { updateCompanyAction } from '../../../redux/actions/company';
import { indexBusinessTypesAction } from '../../../redux/actions/businessTypes';

class General extends Component {
  state = {
    isValid: false,
    params: {
      address: '',
      name: '',
      description: '',
      business_type_id: ''
    }
  }

  componentDidMount() {
    this.props.indexBusinessTypes();
  }

  componentDidUpdate(props) {
    const { address, name, description, business_type } = props.company;
    const { params } = this.state;

    if (name && params['name'] === '') {
      this.setState({
        params: {
          address,
          name,
          description,
          business_type_id: business_type.id
        }
      });
    }
  }

  save() {
    const { id: companyId } = this.props.company;
    const params = {
      address: this.state.params.address,
      name: this.state.params.name,
      description: this.state.params.description,
      business_type_id: this.state.params.business_type_id
    };

    this.props.updateCompany({ companyId, params });
  }

  handleBackgroundImageFinished = imageUrl => {
    const companyId = this.props.company.id;
    const params = { background_image_url: imageUrl };

    return this.props.updateCompany({ companyId, params });
  }

  validateForm() {
    const { name } = this.state.params;
    return name.length
  }

  render() {
    const isValid = this.validateForm();
    const businessTypes = this.props.businessTypes.collection.map(type => { return { value: type.id, name: type.name }; });
    const { company } = this.props;

    return (
      <div className="c-settings-general">
        <div className="c-settings__content__header">
          <h2 className="c-settings__content__header">Ogólne</h2>
        </div>

        <div className="c-settings__content__form">
          <FormInput name="Nazwa firmy" value={this.state.params.name} onChange={value => this.setState({ params: { ...this.state.params, name: value } })} />
          <FormTextarea name="Opis" value={this.state.params.description} onChange={value => this.setState({ params: { ...this.state.params, description: value } })} />
          <FormInput name="Adres" value={this.state.params.address} onChange={value => this.setState({ params: { ...this.state.params, address: value } })} />
          <FormSelect name="Branża" value={this.state.params.business_type_id} options={businessTypes} onChange={value => this.setState({ params: { ...this.state.params, business_type_id: value } })} />
          <FormImageUploader
            name="Zdjęcie w tle"
            dirName="company-images"
            filePrefix={`company-${company.name}-${company.id}-${Date.now()}`}
            imageUrl={company.background_image_url}
            onUploadFinished={this.handleBackgroundImageFinished}
          />

          <div className="c-settings__content__form__actions">
            <button className="button button--medium button--filled-purple" disabled={!isValid} onClick={() => this.save()}>
              { company.isSaving ? <Loader size="medium" color="white" wrapperColor="purple" /> : null }
              Zapisz
            </button>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  company: state.company,
  businessTypes: state.businessTypes,
});

const mapDispatchToProps = dispatch => ({
  updateCompany: data => dispatch(updateCompanyAction(data)),
  indexBusinessTypes: () => dispatch(indexBusinessTypesAction()),
});

export default connect(mapStateToProps, mapDispatchToProps)(General);
