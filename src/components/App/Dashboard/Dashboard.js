import React, { Component } from 'react'
import PageTitle from '../../Ui/PageTitle';
import DashboardCalendar from './Calendar';

class Dashboard extends Component {
  render() {
    return (
      <div className="c-page c-dashboard">
        <PageTitle title="Strona Główna" />

        <div className="c-dashboard__wrapper">
          <DashboardCalendar />
        </div>
      </div>
    );
  }
}

export default Dashboard;
