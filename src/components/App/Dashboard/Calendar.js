import React, { Component } from 'react'
import { Calendar, momentLocalizer } from 'react-big-calendar';
import moment from 'moment';
import 'moment/locale/pl';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import { indexEventsAction } from '../../../redux/actions/events';
import { connect } from 'react-redux';
import { Route } from 'react-router-dom';
import VisitDialog from '../Dialogs/VisitDialog';
import { compose } from 'redux';
import { withRouter } from 'react-router-dom';

const localizer = momentLocalizer(moment);

class DashboardCalendar extends Component {
  componentDidMount() {
    this.props.indexEvents();
  }

  handleSelectEvent = (visit) => {
    this.props.history.push(`/app/dashboard/visits/${visit.id}`);
  }

  render() {
    const { events } = this.props;

    return (
      <div className="c-dashboard__item">
        <h2 className="c-dashboard__item__header">Kalendarz</h2>
        <Calendar
          localizer={localizer}
          events={events}
          culture="pl"
          defaultView="day"
          startAccessor="start"
          endAccessor="end"
          views={['month', 'day', 'work_week']}
          min={new Date(0, 0, 0, 8, 0, 0)}
          max={new Date(0, 0, 0, 17, 0, 0)}
          onSelectEvent={this.handleSelectEvent}
          messages={{
            previous: 'Poprzedni',
            next: 'Następny',
            today: 'Dzisiaj',
            month: 'Miesiąc',
            week: 'Tydzień',
            day: 'Dzień',
            work_week: 'Tydzień',
            date: 'Data',
            time: 'Godzina',
          }}
        />

        <Route path="/app/dashboard/visits/:visitId" component={VisitDialog} />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  events: state.events
});

const mapDispatchToProps = dispatch => ({
  indexEvents: data => dispatch(indexEventsAction(data))
});

export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps)
)(DashboardCalendar);
