import React, { Component } from 'react'
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { deleteSessionAction } from '../../redux/actions/session';
import HomeSvg from '../Ui/Svg/HomeSvg';
import SettingsSvg from '../Ui/Svg/SettingsSvg';
import PeopleSvg from '../Ui/Svg/PeopleSvg';
import MarkerSvg from '../Ui/Svg/MarkerSvg';
import PowerOffSvg from '../Ui/Svg/PowerOffSvg';
import { Redirect } from 'react-router-dom';

class Navigation extends Component {
  signOut() {
    this.props.deleteSession();
  }

  render() {
    const { isLoggedIn } = this.props.session;

    return (
      <nav className="c-navigation">
        { (isLoggedIn === false) && <Redirect to="/signin" /> }

        <div className="c-navigation__list">
          <NavLink to="/app/dashboard" className="c-navigation__list__item" activeClassName={"c-navigation__list__item--active"}>
            <HomeSvg />
          </NavLink>

          <NavLink to="/app/customers" className="c-navigation__list__item" activeClassName={"c-navigation__list__item--active"}>
            <PeopleSvg />
          </NavLink>

          <NavLink to="/app/visits" className="c-navigation__list__item" activeClassName={"c-navigation__list__item--active"}>
            <MarkerSvg />
          </NavLink>

          <NavLink
            to="/app/settings/general"
            className="c-navigation__list__item"
            activeClassName={"c-navigation__list__item--active"}
            isActive={(match, location) => {
              return location.pathname.includes('/app/settings/');
            }}
          >
            <SettingsSvg />
          </NavLink>

          <div className="c-navigation__list__spacer"></div>

          <a className="c-navigation__list__item" onClick={() => this.signOut()}>
            <PowerOffSvg />
          </a>
        </div>
      </nav>
    );
  }
}

const mapStateToProps = state => ({
  session: state.session,
});

const mapDispatchToProps = dispatch => ({
  deleteSession: data => dispatch(deleteSessionAction(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(Navigation);
