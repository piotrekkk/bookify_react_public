import React, { Component } from 'react'
import Dialog from '../Dialog';
import { connect } from 'react-redux';
import { updateVisitAction } from '../../../redux/actions/visits';
import moment from 'moment';
import FormDisplay from '../../Ui/FormDisplay';
import FormLink from '../../Ui/FormLink';
import Loader from '../../Ui/Loader';
import AlertBox from '../../Ui/AlertBox';

class VisitDialog extends Component {
  state = {
    isDeleting: false,
  }

  getVisit() {
    const visitId = this.props.match.params.visitId;
    const visit = this.props.visits.collection.find(visit => visit.id === parseInt(visitId));

    if (visit) {
      return visit;
    } else {
      return {};
    }
  }

  cancelVisit() {
    const visitId = parseInt(this.props.match.params.visitId);
    const params = {
      canceled_at: new Date()
    };

    this.props.updateVisit({ visitId, params });
  }

  render() {
    const { isLoading } = this.props.visits;
    const visit = this.getVisit();
    const customerName = `${visit.user && visit.user.first_name} ${visit.user && visit.user.last_name}`;
    const serviceName = `${visit.service && visit.service.name}`;
    const fullDateName = moment(visit.date).utcOffset('+0000').format('LLLL').charAt(0).toUpperCase() + moment(visit.date).utcOffset('+0000').format('LLLL').slice(1);
    const header = `${serviceName} - ${customerName}`;

    return (
      <Dialog
        header={header}
        isLoadingContent={isLoading}
        exitRoute="/app/visits"
        wrapperClass="c-dialog__wrapper--600"
        actionButton={
          !visit.is_past && !visit.canceled_at && (
            <button className="button button--medium button--filled-red" onClick={() => this.cancelVisit()}>
              { visit.isUpdating ? <Loader size="medium" color="white" wrapperColor="red" /> : null }
              Odwołaj wizytę
            </button>
          )
        }
      >
        { visit.canceled_at && <AlertBox baseClassName="u-margin-bottom--base" type="error" text="Wizyta została odwołana" /> }
        <FormDisplay name="Usługa" value={serviceName} />
        <FormDisplay name="Data" value={fullDateName} />
        <FormLink name="Klient" value={customerName} to={`/app/customers/${visit.user && visit.user.id}`} />
      </Dialog>
    );
  }
}

const mapStateToProps = state => ({
  visits: state.visits
});

const mapDispatchToProps = dispatch => ({
  updateVisit: data => dispatch(updateVisitAction(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(VisitDialog);
