import React, { Component } from 'react'
import Dialog from '../Dialog';
import FormInput from '../../Ui/FormInput';
import FormSelect from '../../Ui/FormSelect';
import Loader from '../../Ui/Loader';
import FormSlotPicker from '../../Ui/FormSlotPicker';
import { createVisitAction } from '../../../redux/actions/visits';
import { indexCustomersAction } from '../../../redux/actions/customers';
import { indexServicesAction } from '../../../redux/actions/services';
import { connect } from 'react-redux';

class NewVisitDialog extends Component {
  state = {
    isNewCustomerMode: false,
    params: {
      service_id: '',
      customer_id: '',
      date: {
        day: '',
        hour: ''
      },
      user: {
        first_name: '',
        last_name: '',
        email: '',
        phone: '',
      }
    }
  }

  componentDidMount() {
    this.props.indexServices();
    this.props.indexCustomers();
  }

  handleChangeCustomerMode = () => {
    this.setState({
      isNewCustomerMode: !this.state.isNewCustomerMode,
      params: {
        ...this.state.params,
        customer_id: ''
      }
    });
  }

  validateForm() {
    const { isNewCustomerMode } = this.state;
    const { service_id, customer_id, date, user } = this.state.params;
    const { first_name, last_name, email, phone } = user;
    const { day, hour } = date;
    const isNewUserValid = !!first_name && !!last_name && !!email && !!phone;
    const isDateValid = !!day && !!hour

    return !!service_id && isDateValid && (isNewCustomerMode ? isNewUserValid : !!customer_id);
  }

  getHeaderText() {
    const { service_id } = this.state.params;
    const service = this.props.services.collection.find(service => service.id == service_id);
    const serviceName = service && service.name;

    return `Nowa wizyta ${serviceName ? '- ' + serviceName : ''}`;
  }

  save() {
    const { isNewCustomerMode } = this.state;
    this.props.createVisit({ params: this.state.params, isNewCustomer: isNewCustomerMode });
  }

  render() {
    const { isNewCustomerMode, params } = this.state;
    const { isCreating } = this.props.visits;
    const header = this.getHeaderText();
    const isValid = this.validateForm();
    const service = this.props.services.collection.find(service => service.id == params.service_id);
    const services = this.props.services.collection.map(type => { return { value: type.id, name: type.name }; });
    const customers = this.props.customers.collection.map(type => { return { value: type.id, name: `${type.first_name} ${type.last_name}` }; });

    return (
      <Dialog
        header={header}
        exitRoute="/app/visits"
        wrapperClass="c-dialog__wrapper--600"
        actionButton={
          <button className="button button--medium button--filled-purple" disabled={!isValid} onClick={() => this.save()}>
            { isCreating ? <Loader size="medium" color="white" wrapperColor="purple" /> : null }
            Zapisz
          </button>
        }
      >
        <FormSelect name="Usługa" value={this.state.params.service_id} options={services} placeholder="Wybierz usługę" onChange={value => this.setState({ params: { ...this.state.params, service_id: value } })} />

        { service &&
          <FormSlotPicker
            name="Data i godzina"
            slots={service && service.slots}
            selected={{ day: params.date.day, hour: params.date.hour }}
            onChange={(day, hour) => this.setState({ params: { ...this.state.params, date: { ...this.state.params.date, day, hour } }})}
          />
        }

        <FormSelect
          name="Klient"
          value={this.state.params.customer_id}
          options={customers}
          placeholder="Wybierz klienta"
          disabled={isNewCustomerMode}
          onChange={value => this.setState({ params: { ...this.state.params, customer_id: value } })}
          rightContent={
            <span className="u-margin-left--auto u-cursor--pointer" onClick={this.handleChangeCustomerMode}>{ isNewCustomerMode ? "Wybierz istniejącego klienta" : "Dodaj nowego klienta" }</span>
          }
        />

        { isNewCustomerMode &&
          <>
            <FormInput name="Imię" value={this.state.params.user.first_name} onChange={value => this.setState({ params: { ...this.state.params, user: { ...this.state.params.user, first_name: value } }})} />
            <FormInput name="Nazwisko" value={this.state.params.user.last_name} onChange={value => this.setState({ params: { ...this.state.params, user: { ...this.state.params.user, last_name: value } }})} />
            <FormInput name="Email" value={this.state.params.user.email} onChange={value => this.setState({ params: { ...this.state.params, user: { ...this.state.params.user, email: value } }})} />
            <FormInput name="Telefon" value={this.state.params.user.phone} onChange={value => this.setState({ params: { ...this.state.params, user: { ...this.state.params.user, phone: value } }})} />
          </>
        }
      </Dialog>
    );
  }
}

const mapStateToProps = state => ({
  customers: state.customers,
  services: state.services,
  visits: state.visits,
});

const mapDispatchToProps = dispatch => ({
  createVisit: data => dispatch(createVisitAction(data)),
  indexServices: () => dispatch(indexServicesAction()),
  indexCustomers: () => dispatch(indexCustomersAction()),
});

export default connect(mapStateToProps, mapDispatchToProps)(NewVisitDialog);
