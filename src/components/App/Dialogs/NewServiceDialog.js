import React, { Component } from 'react'
import Dialog from '../Dialog';
import { connect } from 'react-redux';
import { createServiceAction } from '../../../redux/actions/services';
import Loader from '../../Ui/Loader';
import FormInput from '../../Ui/FormInput';

class NewServiceDialog extends Component {
  state = {
    name: '',
    description: '',
    duration: '',
    price: ''
  }

  save() {
    this.props.createService({ params: this.state });
  }

  validateForm() {
    const { name, description, duration, price } = this.state;
    return !!name && !!description && !!duration && !!price;
  }

  render() {
    const isValid = this.validateForm();
    const { isCreating } = this.props.services;

    return (
      <Dialog
        header={'Nowa usługa'}
        exitRoute="/app/settings/services"
        wrapperClass="c-dialog__wrapper--500"
        actionButton={
          <button className="button button--medium button--filled-purple" disabled={!isValid} onClick={() => this.save()}>
            { isCreating ? <Loader size="medium" color="white" wrapperColor="purple" /> : null }
            Zapisz
          </button>
        }
      >
        <FormInput name="Nazwa" value={this.state.name} onChange={value => this.setState({ ['name']: value })} />
        <FormInput name="Opis" value={this.state.description} onChange={value => this.setState({ ['description']: value })} />
        <FormInput name="Cena" value={this.state.price} onChange={value => this.setState({ ['price']: value })} type="number" />
        <FormInput name="Czas trwania (minuty)" value={this.state.duration} onChange={value => this.setState({ ['duration']: value })} type="number" />
      </Dialog>
    );
  }
}

const mapStateToProps = state => ({
  services: state.services
});

const mapDispatchToProps = dispatch => ({
  createService: data => dispatch(createServiceAction(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(NewServiceDialog);
