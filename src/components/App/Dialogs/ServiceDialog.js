import React, { useState, useMemo } from 'react'
import Dialog from '../Dialog';
import FormInput from '../../Ui/FormInput';
import Loader from '../../Ui/Loader';
import { connect } from 'react-redux';
import { updateServiceAction } from '../../../redux/actions/services';

const ServiceDialog = props => {
  const header = '';
  const [params, setParams] = useState({});
  const { services } = props;
  const serviceId = props.match.params.serviceId;
  const service = useMemo(() => {
    const foundService = services.collection.find(service => service.id === parseInt(serviceId));
    return foundService ? foundService : {};
  }, [services]);
  const isValid = Object.keys(params).every(key => {
    return !!params[key]
  })

  const handleParamsChange = (key, value) => {
    setParams(prevState => ({
      ...prevState,
      [key]: value
    }));
  };

  const save = () => {
    props.updateService({ serviceId: service.id, params });
  };

  return (
    <Dialog
      header={service.name}
      isLoadingContent={services.isLoading}
      exitRoute="/app/settings/services"
      wrapperClass="c-dialog__wrapper--500"
      actionButton={
        <button className="button button--medium button--filled-purple" disabled={!isValid} onClick={() => save()}>
          { service.isUpdating ? <Loader size="medium" color="white" wrapperColor="purple" /> : null }
          Zapisz
        </button>
      }
    >
      <FormInput name="Nazwa" defaultValue={service.name} onChange={value => handleParamsChange('name', value)} />
      <FormInput name="Opis" defaultValue={service.description} onChange={value => handleParamsChange('description', value)} />
      <FormInput name="Cena" defaultValue={service.price} onChange={value => handleParamsChange('price', value)} type="number" />
      <FormInput name="Czas trwania (minuty)" defaultValue={service.duration} onChange={value => handleParamsChange('price', value)} type="number" />
    </Dialog>
  );
};

const mapStateToProps = state => ({
  services: state.services
});

const mapDispatchToProps = dispatch => ({
  updateService: data => dispatch(updateServiceAction(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ServiceDialog);
