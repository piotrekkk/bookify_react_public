import React, { Component } from 'react'
import Dialog from '../Dialog';
import { connect } from 'react-redux';
import FormDisplay from '../../Ui/FormDisplay';
import FormListDisplay from '../../Ui/FormListDisplay';

class CustomerDialog extends Component {
  getCustomer() {
    const customerId = this.props.match.params.customerId;
    return this.props.customers.collection.find(customer => customer.id === parseInt(customerId));
  }

  render() {
    const { isLoading } = this.props.customers;
    const customer = this.getCustomer();
    const customerName = `${customer && customer.first_name} ${customer && customer.last_name}`;

    return (
      <Dialog
        header={customerName}
        isLoadingContent={isLoading}
        exitRoute="/app/customers"
        wrapperClass="c-dialog__wrapper--600"
      >
        { customer && (
          <>
            <FormDisplay name="Klient" value={customerName} />
            <FormDisplay name="Email" value={customer.email} />
            <FormDisplay name="Telefon" value={customer.phone} />
            <FormDisplay name="Ilość wizyt" value={customer.visits_count} />
            <FormListDisplay name="Wizyty" to="/app/visits" items={customer.visits && customer.visits.map(visit => { return { name: visit.service_name, id: visit.id }; })} />
          </>
        )}
      </Dialog>
    );
  }
}

const mapStateToProps = state => ({
  customers: state.customers
});

export default connect(mapStateToProps, null)(CustomerDialog);
