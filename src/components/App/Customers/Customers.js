import React, { Component } from 'react'
import PageTitle from '../../Ui/PageTitle';
import { connect } from 'react-redux';
import { indexCustomersAction } from '../../../redux/actions/customers';
import ListItem from '../../Ui/ListItem';
import ListItemsHeader from '../../Ui/ListItemsHeader';
import Loader from '../../Ui/Loader';
import { Route } from 'react-router-dom';
import CustomerDialog from '../Dialogs/CustomerDialog';
import PageEmptyState from '../../Ui/PageEmptyState';
import CustomersEmptySvg from '../../Ui/Svg/CustomersEmptySvg';

class Customers extends Component {
  componentDidMount() {
    this.props.indexCustomers();
  }

  render() {
    const { customers } = this.props;

    return (
      <div className="c-page">
        <PageTitle title="Klienci" />

        { !customers.isLoading && customers.collection.length > 0 &&
          <ListItemsHeader>
            <span>Klient</span>
            <span>Telefon</span>
            <span>Ilość wizyt</span>
          </ListItemsHeader>
        }

        { !customers.isLoading && customers.collection.length === 0 &&
          <PageEmptyState
            title="Nie masz jeszcze żadnych klientów!"
            description="Pojawią się tutaj klienci, którzy umówili się do ciebie na wizytę."
            image={<CustomersEmptySvg style={{width: '100%', maxWidth: 500}} />}
          />
        }

        { (customers.isLoading && !customers.collection.length) && <Loader size="medium" color="purple" wrapperColor="white" isStatic={true} /> }

        { customers.collection.map((customer, index) =>
            <ListItem key={index} to={`/app/customers/${customer.id}`}>
              <span>{ customer.first_name } { customer.last_name }</span>
              <span>{ customer.phone }</span>
              <span>{ customer.visits_count }</span>
            </ListItem>
          )
        }

        <Route path="/app/customers/:customerId" component={CustomerDialog} />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  customers: state.customers
});

const mapDispatchToProps = dispatch => ({
  indexCustomers: data => dispatch(indexCustomersAction(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(Customers);
