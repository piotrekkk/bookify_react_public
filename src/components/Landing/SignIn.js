import React, { Component } from 'react';
import Header from './Header';
import { createSessionAction } from '../../redux/actions/session';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

class SignIn extends Component {
  state = {
    email: '',
    password: '',
  };

  signIn() {
    this.props.createSession({
      params: {
        email: this.state.email,
        password: this.state.password
      }
    });
  }

  render () {
    const isDisabled = !this.state.email || !this.state.password;
    const { isLoggedIn } = this.props.session;

    return (
      <div className="c-signin">
        { isLoggedIn && <Redirect to="/app/dashboard" /> }

        <Header />

        <div className="c-signin__content">
          <div className="h-inner">
            <h1>Zaloguj się</h1>

            <div className="c-signin__form">
              <label>
                <span>Email</span>
                <input type="email" className="landing-input" value={this.state.email} onChange={(e) => this.setState({ email: e.target.value })} />
              </label>

              <label>
                <span>Hasło</span>
                <input type="password" className="landing-input" value={this.state.password} onChange={(e) => this.setState({ password: e.target.value })} />
              </label>

              <button className="landing-button landing-button--filled landing-button--filled-purple" disabled={isDisabled} onClick={() => this.signIn()}>Zaloguj się</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  session: state.session,
});

const mapDispatchToProps = dispatch => ({
  createSession: data => dispatch(createSessionAction(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);
