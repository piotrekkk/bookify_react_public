import React, { Component } from 'react'

class Header extends Component {
  render() {
    return (
      <header className="c-landing-header">
        <div className="h-inner">
          <a href={process.env.REACT_APP_RAILS_APP_HOST} className="c-landing-header__logo">Bookify</a>
        </div>
      </header>
    );
  }
}

export default Header;
